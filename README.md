# Control Panel for Pi-Hole

This adds a URL to enable/disable Pi-Hole on your network. It could be used to have a link on your phone/computer to
quickly enable/disable. This is very insecure since I want to have a one-click way to perform these actions. You could
also go to the Pi-Hole dashboard and use that interface, which is more secure (need the password), but is also more
clicks.

The URLs will be:

* Enable: http://pi.hole:5000/enable
* Disable: http://pi.hole:5000/disable (will disable for 5 minutes)

## Security

**Disclaimer**: This is very insecure and should not be used unless you understand the risks.

If you still want to use it, know that anyone on your network could enable or disable Pi-Hole. This probably isn't an
issue, but be warned. This could get way worse if you expose the Pi-Hole server to the world.

## Running

`python3 app.py`

## Running Automatically

One way to get this to run at startup is to use a Cron job:

* `crontab -e`
* `@reboot python3 /home/pi/pihole-web-interface/app.py`

A probably better way would be in `init.d`.

## Dependencies

`sudo apt-get install python3-flask`

This may already be installed on your pi, depending on the distro
