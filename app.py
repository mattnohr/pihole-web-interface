from flask import Flask, render_template
from subprocess import call

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('main.html', message='Hello World')

@app.route('/disable')
def disable():
    call(['pihole', 'disable', '5m'])
    return render_template('main.html', message='Disabed PiHole for 5 minutes')

@app.route('/enable')
def enable():
    call(['pihole', 'enable'])
    return render_template('main.html', message='Enabled PiHole')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
